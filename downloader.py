import os
import urllib.request
from urllib.parse import urlparse
import logging

downloads_DIR = 'Downloads/'
total_read = 0
success = 0
fail = 0

# Read TXT
text_file = open("urls.txt", "r")
lines = text_file.readlines()
text_file.close()
total_read = len(lines)

# Download URLs
with open('urls.txt', 'r') as reader:
    line = reader.readline()
    line = line.rstrip()
    while line != '':  # The EOF char is an empty string
        line = reader.readline()
        line = line.rstrip()
        if not os.path.exists(downloads_DIR):
            os.makedirs(downloads_DIR)
        urlname = urlparse(line)
        urlname = urlname.path
        filename = os.path.join(downloads_DIR, (urlname.strip('/') + ".jpg"))
        if not os.path.isfile(filename):
            logging.basicConfig(filename="download.log", level=logging.INFO)
            try:
                urllib.request.urlretrieve(line, filename)
                logging.info("Downloading :" + line)
            except Exception as inst:     
                logging.info(str(line) + " - Unknown Error - " + str(inst))
                fail = fail + 1
            success = success + 1
print("Lines Read: " + str(total_read))
print("Downloaded: " + str(success))
print("Failed Download: " + str(fail))